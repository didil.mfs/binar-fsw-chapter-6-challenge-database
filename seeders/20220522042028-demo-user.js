"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    let users = [
      {
        username: "maselon",
        password: "maselon",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        username: "masbezos",
        password: "masbezos",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ];
    await queryInterface.bulkInsert("user_games", users);

    const userIDs = await queryInterface.sequelize.query(`SELECT user_id from user_games;`);
    const userIDRows = userIDs[0];

    let user_biodata = [
      {
        fullname: "Elon Musk",
        phone: 6243214321,
        email: "elonmusk@tesla.com",
        createdAt: new Date(),
        updatedAt: new Date(),
        userGameUserId: userIDRows[0].user_id,
      },
      {
        fullname: "Jeff Bezos",
        phone: 6212341234,
        email: "jeffbezos@amazon.com",
        createdAt: new Date(),
        updatedAt: new Date(),
        userGameUserId: userIDRows[1].user_id,
      },
    ];
    await queryInterface.bulkInsert("user_game_biodata", user_biodata);

    let user_history = [
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        userGameUserId: userIDRows[0].user_id,
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        userGameUserId: userIDRows[1].user_id,
      },
    ];
    await queryInterface.bulkInsert("user_game_histories", user_history);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("user_games", null, {});
    await queryInterface.bulkDelete("user_game_biodata", null, {});
  },
};
