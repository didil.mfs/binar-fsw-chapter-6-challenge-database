const express = require("express");
const router = express.Router();
let users = require("../database/user.json");
const { user_game, user_game_biodata, user_game_history } = require("../models");

router.use(function timeLog(req, res, next) {
  console.log("Time: ", Date.now());
  next();
});

// Routing
router.get("/", (req, res) => {
  res.render("home");
});
router.get("/play", (req, res) => {
  res.render("play");
});
router.get("/login", (req, res) => {
  res.render("login");
});

// Auth
router.post("/login", (req, res) => {
  let request = req.body;
  let usersData = users;
  for (let i = 0; i < usersData.length; i++) {
    const user = usersData[i];
    if (request.username == user.username && request.password == user.password) {
      return res.redirect("/dashboard");
    }
  }
  user_game.findAll().then((usersDB) => {
    console.log(usersDB);
    for (let i = 0; i < users.length; i++) {
      const user = usersDB[i];
      console.log(user);
      console.log(request.username);
      console.log(request.password);
      if (request.username == user.username && request.password == user.password) {
        console.log("found");
        return res.redirect("/play");
      }
    }
    return res.send("user not found");
  });
});

// Dashboard
router.get("/dashboard", (req, res) => {
  user_game.findAll({ include: [user_game_biodata, user_game_history] }).then((users) => {
    res.render("dashboard", { users });
  });
});

router.post("/dashboard", (req, res) => {
  user_game
    .create(
      {
        username: req.body.username,
        password: req.body.password,
        user_game_biodatum: {
          fullname: req.body.fullname,
          email: req.body.email,
          phone: req.body.phone,
        },
        user_game_history: {
          win: 0,
          lose: 0,
        },
      },
      { include: [{ association: user_game.hasOne(user_game_biodata) }, { association: user_game.hasOne(user_game_history) }] }
    )
    .then((user) => {
      res.status(200);
      res.redirect("/dashboard");
    });
});

router.delete("/dashboard", (req, res) => {
  console.log(req.body.username);
  user_game.destroy({ where: { username: req.body.username } }).then((user) => {
    console.log("User ", req.body.username, " deleted");
    res.status(200);
    res.redirect("/dashboard");
  });
});

router.get("/dashboard/update/biodata/:username", (req, res) => {
  user_game.findOne({ where: { username: req.params.username }, include: [user_game_biodata] }).then((user) => {
    res.render("dashboard_update_biodata", { user });
    console.log(user);
    // res.send(user);
  });
});

router.post("/dashboard/update/biodata/:username", (req, res) => {
  user_game.findOne({ where: { username: req.params.username } }).then((user) => {
    user_game_biodata
      .update(
        {
          fullname: req.body.fullname,
          email: req.body.email,
          phone: req.body.phone,
        },
        { where: { userGameUserId: user.user_id } }
      )
      .then((user) => {
        console.log(user);
        res.status(200);
        res.redirect("/dashboard");
      });
  });
});

router.get("/dashboard/update/password/:username", (req, res) => {
  user_game.findOne({ where: { username: req.params.username } }).then((user) => {
    res.render("dashboard_update_password", { user });
    console.log(user);
    // res.send(user);
  });
});

router.post("/dashboard/update/password/:username", (req, res) => {
  user_game.findOne({ where: { username: req.params.username } }).then((user) => {
    user_game
      .update(
        {
          password: req.body.password,
        },
        { where: { user_id: user.user_id } }
      )
      .then((user) => {
        console.log(user);
        res.status(200);
        res.redirect("/dashboard");
      });
  });
});

// API User Data
router.get("/api/v1/users", (req, res) => {
  res.status(200).json(users);
});

router.get("/api/v1/users/:id", (req, res) => {
  const user = users.find((i) => i.id === +req.params.id);
  res.status(200).json(user);
});

router.post("/api/v1/users/", (req, res) => {
  // Destruct req.body
  const { username, password, fullname } = req.body;
  // Get ID
  const id = users[users.length - 1].id + 1;
  const user = {
    id,
    username,
    password,
    fullname,
  };
  // Simpan ke users array
  users.push(user);
  res.status(201).json(user);
});

router.put("/api/v1/users/:id", (req, res) => {
  let user = users.find((i) => i.id === +req.params.id);
  const params = {
    username: req.body.username,
    password: req.body.password,
    fullname: req.body.fullname,
  };
  user = { ...user, ...params };
  users = users.map((i) => (i.id === user.id ? user : i));
  res.status(200).json(user);
});

router.delete("/api/v1/users/:id", (req, res) => {
  users = users.filter((i) => i.id !== +req.params.id);
  res.status(200).json({
    message: `User dengan id ${req.params.id} sudah berhasil dihapus`,
  });
});

module.exports = router;
