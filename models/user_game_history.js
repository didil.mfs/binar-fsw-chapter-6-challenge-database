"use strict";
const { Model } = require("sequelize");
const user_game = require("./user_game");
module.exports = (sequelize, DataTypes) => {
  class user_game_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // // define association here
      // this.belongsTo(models.user_game, {
      //   onDelete: "CASCADE",
      // });
    }
  }
  user_game_history.init(
    {
      win: {
        type: DataTypes.BIGINT,
        defaultValue: 0,
        allowNull: true,
      },
      lose: {
        type: DataTypes.BIGINT,
        defaultValue: 0,
        allowNull: true,
      },
    },
    {
      sequelize,
      modelName: "user_game_history",
    }
  );
  return user_game_history;
};
