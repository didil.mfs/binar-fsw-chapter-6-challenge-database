"use strict";
const { Model } = require("sequelize");
const user_game = require("./user_game");
module.exports = (sequelize, DataTypes) => {
  class user_game_biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // // define association here
      // this.belongsTo(models.user_game, {
      //   onDelete: "CASCADE",
      // });
    }
  }
  user_game_biodata.init(
    {
      fullname: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      phone: {
        type: DataTypes.BIGINT,
        allowNull: true,
      },
    },
    {
      sequelize,
      modelName: "user_game_biodata",
      tableName: "user_game_biodata",
    }
  );
  return user_game_biodata;
};
