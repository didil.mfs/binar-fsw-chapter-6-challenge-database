"use strict";
const { Model } = require("sequelize");
const user_game_biodata = require("./user_game_biodata");
const user_game_history = require("./user_game_history");
module.exports = (sequelize, DataTypes) => {
  class user_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasOne(models.user_game_history, {
        onDelete: "CASCADE",
      });
      this.hasOne(models.user_game_biodata, {
        onDelete: "CASCADE",
      });
    }
  }
  user_game.init(
    {
      user_id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      username: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "user_game",
    }
  );
  return user_game;
};
